"use client"; // use client component because "useState" hook not supported server component
import { useRouter } from "next/navigation";
import React, { FormEventHandler, useState } from "react";
import { AiOutlinePlus } from "react-icons/ai";
import { addTodo } from "../../../api";
import Modal from "../modal";
import { v4 as uuidv4 } from "uuid";

export interface AddTaskProps {}

export default function AddTask(props: AddTaskProps) {
  const [openModal, setOpenModal] = useState(false);
  const [newTaskValue, setNewTaskValue] = useState("");
  const router = useRouter();

  const handleSubmitNewTodo: FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();
    setNewTaskValue("");
    setOpenModal(false);

    // console.log(newTaskValue);

    await addTodo({
      id: uuidv4(),
      text: newTaskValue,
    });
    router.refresh();
  };
  return (
    <div>
      <button
        onClick={() => setOpenModal(true)}
        className="btn btn-primary w-full"
      >
        Add new task
        <AiOutlinePlus className="ml-2" size={18} />
      </button>
      <Modal openModal={openModal} setOpenModal={setOpenModal}>
        <form onSubmit={handleSubmitNewTodo}>
          <h3 className="font-bold text-lg">Add new task</h3>
          <div className="modal-action">
            <input
              value={newTaskValue}
              onChange={(e) => setNewTaskValue(e.target.value)}
              type="text"
              placeholder="Type new task here"
              className="input input-bordered input-md w-full"
              required={true}
            />
            <button className="btn" type="submit">
              Submit
            </button>
          </div>
        </form>
      </Modal>
    </div>
  );
}
