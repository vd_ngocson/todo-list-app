/* eslint-disable react/no-unescaped-entities */
import React from "react";

export interface ModalProps {
  openModal: boolean;
  setOpenModal: (open: boolean) => boolean | void;
  children: React.ReactNode;
}

export default function Modal({
  openModal,
  setOpenModal,
  children,
}: ModalProps) {
  return (
    <div className={`modal ${openModal ? "modal-open" : ""}`}>
      <div className="modal-box relative">
        <label
          onClick={() => setOpenModal(false)}
          className="btn btn-sm btn-circle absolute right-2 top-2"
        >
          ✕
        </label>
        {children}
      </div>
    </div>
  );
}
