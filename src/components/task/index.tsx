"use client";
import React, { FormEventHandler, useState } from "react";
import { ITask } from "../../../types/tasks";
import { FiEdit, FiTrash2 } from "react-icons/fi";
import Modal from "../modal";
import { useRouter } from "next/navigation";
import { deleteTodo, editTodo } from "../../../api";

export interface TaskProps {
  task: ITask;
}

export default function Task({ task }: TaskProps) {
  const router = useRouter();
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [taskToEdit, setTaskToEdit] = useState(task.text);

  const handleSubmitEditTodo: FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();
    setOpenModalEdit(false);

    // console.log(newTaskValue);

    await editTodo({
      id: task.id,
      text: taskToEdit,
    });
    router.refresh();
  };

  const handleDeleteTask = async (id: string) => {
    await deleteTodo(id);
    setOpenModalDelete(false);
    router.refresh();
  };
  return (
    <tr key={task.id}>
      <td>{task.text}</td>
      <td className="flex gap-3">
        {/* Action edit */}
        <FiEdit
          onClick={() => setOpenModalEdit(true)}
          cursor="pointer"
          className="text-blue-500"
          size={25}
        />
        <Modal openModal={openModalEdit} setOpenModal={setOpenModalEdit}>
          <form onSubmit={handleSubmitEditTodo}>
            <h3 className="font-bold text-lg">Edit task</h3>
            <div className="modal-action">
              <input
                value={taskToEdit}
                onChange={(e) => setTaskToEdit(e.target.value)}
                type="text"
                placeholder="Type new task here"
                className="input input-bordered input-md w-full"
                required={true}
              />
              <button className="btn" type="submit">
                Submit
              </button>
            </div>
          </form>
        </Modal>

        {/* Action Delete */}
        <FiTrash2
          onClick={() => setOpenModalDelete(true)}
          cursor="pointer"
          className="text-red-500"
          size={25}
        />
        <Modal openModal={openModalDelete} setOpenModal={setOpenModalDelete}>
          <h3 className="text-lg">Are you sure? You want delete this task??</h3>
          <div className="modal-action">
            <button onClick={() => handleDeleteTask(task.id)} className="btn">
              Yes
            </button>
          </div>
        </Modal>
      </td>
    </tr>
  );
}
